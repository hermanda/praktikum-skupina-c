# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import curve_fit
import numpy as np
from os import listdir
from os.path import isfile, join
import math
import scipy.optimize as opt
import sys
import os
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline


matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='Arial')
# matplotlib.rc("font", **{"family": "DejaVu Sans"})
params = {'backend': 'ps',
      'text.usetex': True,
      'text.latex.unicode':True
}
plt.rcParams.update(params)

args = str(sys.argv)
graphs_all = False
if "--graphs_all" in args:
    graphs_all = True

def convert_str(value):
    if type(value) == str:
        # Ignore errors even if the string is not proper UTF-8 or has
        # broken marker bytes.
        # Python built-in function unicode() can do this.
        value = unicode(value, "utf-8", errors="ignore")
    else:
        # Assume the value object has proper __unicode__() method
        value = unicode(value)
    return value

def fitFunc(t, a, b):
    return x


"""
def f_y(y):
    return 1/(2*3.141526*y*1000)**2
"""


def f_x(x, a):
    return 2 / (3 * a * np.sqrt(x))


def f_y(y):
    return y


def gauss(x, a, b, c):
    return np.exp(-np.power(x - a, 2.0) / (2 * np.power(abs(b), 2.0))) * c


# f2 = interp1d(data_x, data_y, kind='cubic')
# plt.plot(x_lin, f2(x_lin), 'k--', label='kubicka interpolacia')


def frange(x, y, num):
    h = (y - x) / float(num)
    # print h, num, x, y
    while x < y:
        yield float(x)
        x += h


def fe2str(f, e, dec_pl):
    sign, f = int(round(f / abs(f))), abs(f)
    order_num = np.log(f) / np.log(10)
    order_num = int(math.floor(order_num))
    if order_num == 0:
        return (
            str(round(sign * f / 10 ** order_num, dec_pl)).replace(".", ",")
            + "$\pm$"
            + str(round(e / 10 ** order_num, dec_pl)).replace(".", ",")
        )
    else:
        return (
            "("
            + str(round(sign * f / 10 ** order_num, dec_pl)).replace(".", ",")
            + "$\pm$"
            + str(round(e / 10 ** order_num, dec_pl)).replace(".", ",")
            + ")"
            + ".$10^{"
            + str(order_num)
            + "}$"
        )


def f2str(f, dec_pl):
    sign, f = int(round(f / abs(f))), abs(f)
    order_num = np.log(f) / np.log(10)
    order_num = int(math.floor(order_num))
    return (
        str(round(sign * f / 10 ** order_num, dec_pl)).replace(".", ",")
        + ".$10^{"
        + str(order_num)
        + "}$"
    )


def parm2label(params, err, var_names, dig_pl):
    s = ""
    for i in xrange(len(params)):
        if isinstance(dig_pl, list):
            d = dig_pl[i]
        else:
            d = dig_pl
        p, e, n = params[i], np.sqrt(err[i, i]), var_names[i]
        s += n + " = " + fe2str(p, e, d) + "\n"
    return s[:-1]


def process_num(num):
    if "," in num:
        return float(num.replace(",", "."))
    elif "." in num:
        return float(num)
    else:
        return int(num)


# vln. delka, namer. spectr., nominal spectr., spektr. odezva
label_calibration = (
    "vlnová delka",
    "namerané spektrum.",
    "nomin8lne spektrum",
    "spektrálna odozva",
)
units_calibration = ()
file_path = "../calibration.csv"
data_calibration = []
with open(file_path, "r") as f:
    for i in range(3):
        f.readline()

    for row in f:
        row = row.replace("\n", "")
        row = row.split("\t")
        if row[0] == "":
            continue
        row = list(map(process_num, row))
        data_calibration.append(row)

data_calibration = np.array(data_calibration)
print(data_calibration)

label_converted = (
    "VIS",
    "UV-A",
    "UV-B",
    "UV-A sklo",
    "UV-A olej",
    "UV-B sklo",
    "UV-B olej",
)
units_converted = "[mW / m2 nm]"
file_path = "../data_converted.csv"
data_converted = []
with open(file_path, "r") as f:
    for i in range(3):
        f.readline()

    for row in f:
        row = row.replace("\n", "")
        row = row.split("\t")
        if row[0] == "":
            continue
        row = list(map(process_num, row))
        data_converted.append(row)

data_converted = np.array(data_converted)

file_path = "../data_notconverted.csv"
data_notconverted = []
with open(file_path, "r") as f:
    for i in range(3):
        f.readline()

    for row in f:
        row = row.replace("\n", "")
        row = row.split("\t")
        if row[0] == "":
            continue
        row = list(map(process_num, row))
        data_notconverted.append(row)

data_notconverted = np.array(data_notconverted)

file_path = "../eryth_melat.dat"
data_em = []
with open(file_path, "r") as f:
    for row in f:
        row = row.replace("\n", "")
        row = row.split("\t")
        if row[0] == "":
            continue
        row = list(map(process_num, row))
        data_em.append(row)

data_em = np.array(data_em)

data_len = data_converted.shape[0]
wavelength = data_calibration[:, 0]

if graphs_all:
    ### Spektralna hustota oziarenia
    fig, ax1 = plt.subplots()
    t = np.arange(0.01, 10.0, 0.01)
    ax1.plot(wavelength, data_calibration[:, 2], 'k--', label="Spektr. hust. ožiar.")
    ax1.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=10)
    ax1.set_ylabel(r"Spektrálna hustota ožiarenia $(\mathrm{mW}/\mathrm{m^2 nm})$", fontsize=12, color='k')

    ax2 = ax1.twinx()
    ax2.plot(wavelength, data_calibration[:, 1]*10**5, 'k-', label="Det. signál")
    ax2.set_ylabel(r"Detekovaný signál $\times 10^{-5}\:(\mathrm{V})$", fontsize=12)

    handles1, labels1 = ax1.get_legend_handles_labels()
    handles2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(handles1+handles2, labels1+labels2, loc='upper left')

    fig.tight_layout()
    plt.savefig("Graph01.png", size=(4, 3), dpi=600)

    ### Spektralna odozva aparatury
    plt.figure()
    plt.plot(wavelength, 1/data_calibration[:, 3]/10**6, 'k-')
    plt.xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=10)
    plt.ylabel(r"Spektrálna odozva aparatúry $\times 10^{-6}\:(\mathrm{mW}/\mathrm{m^2 nm V})$", fontsize=12) #   [V m2 nm / mW ]
    plt.savefig("Graph02.png", size=(4, 3), dpi=600)

wavelength = data_calibration[:data_converted.shape[0], 0]
scale = 1/data_calibration[:data_converted.shape[0], 3]

data = np.zeros_like(data_notconverted)
for i in range(data_notconverted.shape[1]):
    data[:, i] = data_notconverted[:, i]/data_calibration[:data_notconverted.shape[0], 3]

print(data_notconverted[50, 0], data_notconverted[50, 3])

y_label = "Spektrálna hustota ožiarenia"
y_units1 = r"$\:(\mathrm{mW}/\mathrm{m^2 nm V})$"
y_units2 = r"$\:(\mathrm{mW}/\mathrm{m^2 nm})$"

if graphs_all:
    ### VIS
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 0], 'k-')
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=10)
    ax.set_ylabel(y_label+" VIS"+y_units2, fontsize=10) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    plt.savefig("Graph03.png", size=(4, 3), dpi=600)

    ### UV-A
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 1], 'k-')
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=10)
    ax.set_ylabel(y_label+" UV-A"+y_units2, fontsize=10) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    plt.savefig("Graph04.png", size=(4, 3), dpi=600)

    ### UV-B
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 2], 'k-')
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=10)
    ax.set_ylabel(y_label+" UV-B"+y_units2, fontsize=10) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    plt.savefig("Graph05.png", size=(4, 3), dpi=600)

    ### VIS, UV-A, UV-B
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 0], 'k-', label="VIS", alpha=0.7, linewidth=1)
    ax.plot(wavelength, data[:, 1], 'g-', label="UV-A", alpha=0.7, linewidth=1)
    ax.plot(wavelength, data[:, 2], 'r-', label="UV-B", alpha=0.7, linewidth=1)
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=12)
    ax.set_ylabel(y_label+" "+y_units2, fontsize=12) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.legend(loc='upper right')
    plt.savefig("Graph06.png", size=(4, 3), dpi=600)

    ### UV-A sklo, UV-A olej
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 3], 'k-', label="UV-A sklo", alpha=0.7, linewidth=1)
    ax.plot(wavelength, data[:, 4], 'b-', label="UV-A sklo + olej", alpha=0.7, linewidth=1)
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=12)
    ax.set_ylabel(y_label+" "+y_units2, fontsize=12) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.legend(loc='upper right')
    plt.savefig("Graph07.png", size=(4, 3), dpi=600)

    ### UV-A sklo, UV-A olej
    fig, ax = plt.subplots()
    ax.plot(wavelength, data[:, 5], 'k-', label="UV-B sklo", alpha=0.7, linewidth=1)
    ax.plot(wavelength, data[:, 6], 'b-', label="UV-B sklo + olej", alpha=0.7, linewidth=1)
    ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=12)
    ax.set_ylabel(y_label+" "+y_units2, fontsize=12) #   [V m2 nm / mW ]
    ax.set_yscale('log')
    ax.grid()
    ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                    color='green', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                    color='red', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                    color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
    ax.legend(loc='upper right')
    plt.savefig("Graph08.png", size=(4, 3), dpi=600)

# UV-A rozdiel medzi sklom a sklo + olej

rel_eff_A = data[:, 3] / data[:, 4]
rel_eff_B = data[:, 5] / data[:, 6]

fig, ax = plt.subplots()
ax.plot(wavelength, rel_eff_A, 'k-', label="UV-A sklo", alpha=0.7, linewidth=1)
ax.plot(wavelength, rel_eff_B, 'b-', label="UV-B sklo", alpha=0.7, linewidth=1)
ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=12)
ax.set_ylabel("Relatívna účinnosť oleja", fontsize=12) #   [V m2 nm / mW ]

ax.grid()
ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                color='green', alpha=0.2, transform=ax.get_xaxis_transform())
ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                color='red', alpha=0.2, transform=ax.get_xaxis_transform())
ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
ax.legend(loc='upper right')
plt.savefig("Graph09.png", size=(4, 3), dpi=600)

a, b, c = 280-250, 320-250, 400-250
print("UV-A relative effectiveness min max:", min(rel_eff_A[b:c+1]), max(rel_eff_A[b:c+1]))
print("UV-A relative effectiveness min max:", min(rel_eff_A[a:b+1]), max(rel_eff_A[a:b+1]))
print("UV-B relative effectiveness min max:", min(rel_eff_B[b:c+1]), max(rel_eff_B[b:c+1]))
print("UV-B relative effectiveness min max:", min(rel_eff_B[a:b+1]), max(rel_eff_B[a:b+1]))

# akcne spektra

data_len = data_em.shape[0]

fig, ax = plt.subplots()
ax.plot(wavelength[:data_len], data_em[:, 0], 'k-', label="erytéma", alpha=0.7, linewidth=1)
ax.plot(wavelength[:data_len], data_em[:, 1], 'b-', label="melanom", alpha=0.7, linewidth=1)
ax.set_xlabel(r"$\lambda\:(\mathrm{nm})$", fontsize=12)
ax.set_ylabel("Relatívna spektr. účinnosť ožiarenia", fontsize=12) #   [V m2 nm / mW ]

ax.grid()
ax.fill_between(wavelength, 0, 1, where=np.where((320 < wavelength) & (wavelength <= 400), True, False),
                color='green', alpha=0.2, transform=ax.get_xaxis_transform())
ax.fill_between(wavelength, 0, 1, where=np.where((280 < wavelength) & (wavelength <= 320), True, False),
                color='red', alpha=0.2, transform=ax.get_xaxis_transform())
ax.fill_between(wavelength, 0, 1, where=np.where((100 < wavelength) & (wavelength <= 280), True, False),
                color='gray', alpha=0.2, transform=ax.get_xaxis_transform())
ax.legend(loc='upper right')
# plt.show()
plt.savefig("Graph10.png", size=(4, 3), dpi=600)
