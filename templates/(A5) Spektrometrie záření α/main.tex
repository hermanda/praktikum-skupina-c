\documentclass[a4paper]{article}

\usepackage[slovak]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{hyperref}
\usepackage{gensymb} 
\usepackage{tabularx}
\renewcommand{\refname}{Zoznam použitej literatúry}
\usepackage{indentfirst}
\newfloat{customgraph}{Hhntbp}{grp}
\floatname{customgraph}{Graf}
\floatstyle{plaintop}
\newfloat{customtables}{Hhntbp}{tbl}
\floatname{customtables}{Tabuľka}
\usepackage{multirow}

\usepackage[total={17cm,25cm}, top=3cm, left=2.5cm, right=2.5cm, includefoot]{geometry}
\usepackage{hhline}
\pagestyle{fancy}
\lhead{Praktikum IV}
\rhead{Daniel Herman}
\usepackage[shortlabels]{enumitem}

 
\begin{document}
\setcounter{page}{2}
\section*{Pracovné úlohy}
\begin{enumerate}
  \item Vykonajte energetickú kalibráciu $\alpha$-spektrometra a určite jeho rozlíšenie.
  \item Určite absolútnu aktivitu kalibračného rádioizotopu $^{241}$Am.
  \item Zmerajte závislosť ionizačných strát $\alpha$-častíc na tlaku vzduchu $\Delta T = \Delta T(P)$.
  \item Určite špecifické ionizačné straty $\alpha$-častíc vo vzduchu pri normálnom tlaku $-\frac{d T}{d x} = f(T)$. Porovnajte túto závislosť so závislosťou získanou pomocou empirickej formuly pre dolet $\alpha$-častíc vo vzduchu za normálnych podmienok.
  \item Určite energie $\alpha$-častíc vylietavajúcich zo vzorky obsahujúcej izotop $^{239}$Pu
        a prímes izotopu $^{238}$Pu a prirovnajte ich s etablovanými hodnotami. Stanovte relatívne zastúpenie izotopu $^{238}$Pu s presnosťou lepšou, než 10\%, ak sú $T_{1/2}(^{238}$Pu$)$ = 87,71 yr a  $T_{1/2}(^{239}$Pu$)$ = 24,13.10$^{3}$ yr.
\end{enumerate}
\section{Teoretická časť}
\subsection*{Absolútna aktivita žiariča $^{241}$Am}
\indent Absolútna aktivita žiariča A je počet častíc, ktoré emituje za sekundu. Tú možno určiť z počtu zaznamenaných častíc, $N$ v detektore za čas $t$. Potom absolútnu aktivitu vieme určiť ako
\begin{equation}
  A=4 \pi \frac{l^{2}}{S} \frac{N}{t},
\end{equation}
pričom $l$ je vzdialenosť stredu detektora od bodového zdroja žiarenia a $S$ je plocha detektora. Tento vzťah je iba aproximáciou, pretože sa jedná o výsek z sféry o polomeru $l$, avšak detektor použitý v tomto experimente je plochý.
\subsection*{Ionizačné straty kinetickej energie $\alpha$-častíc}
\indent Špecifická ionizačná strata $f(T)$ je definovaná vzťahom
\begin{equation}
  f(T) = -\frac{d T}{dx} (T).
\end{equation}
Zmena tlaku v aparatúre a zmena vzdialenosti žiariča od detektoru má rovnaký dopad na zmenu zaznamenanej kinetickej energie $\alpha$-častíc. Nech $p_a$ je atmosferický tlak, $x_a$ je efektívna dráha. Zo znalosti tlaku $p$ v aparatúre a vzdialenosti žiariča od detektoru možno vyjadriť $x_a$
\begin{equation}
  p_a x_a = p x \Leftrightarrow x_a = \frac{p x}{p_a}.
\end{equation}
Chybu pre takto vyjadrenú vzdialenosť určíme na základe nepriameho merania (4.16)[1]
\begin{equation}
  \sigma_{x_a }^2 = \left(\frac{x}{p_{a}}\right)^2\sigma_{p}^{2}+\left(\frac{p}{p_{a}}\right)^2\sigma_{x}^{2}+\left(\frac{p x}{p_{a}^{2}}\right)^2\sigma_{p_a}^{2}.
\end{equation}
Výhodou zmeny tlaku oproti zmene efektívnej dráhy je fakt, že pri tejto metóde nemeníme priestorový uhol a teda naša systematická chyba sa nebude meniť naprieč celým meraním. \\
\indent Vzdialenosť $R$ po ktorej častica sa s počiatočnou energiou $T_0$ zastaví sa nazýva zbytkový dolet
\begin{equation}
  R=\int_{0}^{R} \mathrm{d} x=\int_{0}^{T_{0}} \frac{\mathrm{d} T}{f(T)}=R\left(T_{0}\right).
\end{equation}
Pre zbytkový dolet $\alpha$-častíc vo vzduchu za normálnych podmienok máme danú empirickú závislosť [2]
\begin{equation}
  R=\xi T_{0}^{3 / 2}, \quad \xi=0.31 \mathrm{cm} \cdot \mathrm{MeV}^{-3 / 2}, \quad T_{0} \in\langle 4,7\rangle. \mathrm{MeV}
\end{equation}
Po integrácii vzťahu (3) sme obdržali špecifickú ionizačný úbytok
\begin{equation}
  f(T)=\frac{2}{3 \xi \sqrt{T}}.
\end{equation}
\subsection*{Pomerná aktivita}
Majme teda vzorku zloženú z dvoch izotopov, $I$ resp. $J$, s pomerným zastúpením $p_I = \frac{c_I}{c_J+c_I}$ resp. $p_J = \frac{c_J}{c_J+c_I}$, s aktivitami $A_I$ resp. $A_J$ a polčasmi rozpadu $t_I$ resp. $t_J$. Potom platí vzťah
\begin{equation}
  p_{I}=\frac{A_{I}}{A_{I}+\frac{t_{I}}{t_{I}} A_{J}}.
\end{equation}
Predpokladajme, že polčasy rozpadov sú určené presne, poznáme teda chybu $A_I$ resp. $A_J$ značenú ako $\sigma_I$ resp. $\sigma_J$. Potom celkovú chybu pomernej aktivity určíme (4.16)[1] ako
\begin{equation}
  \sigma_{p}^{2}=\sigma_{I}^{2}\left(\frac{A_{J} \frac{t_{J}}{t_{I}}}{\left(A_{I}+\frac{t_{J}}{t_{I}} A_{J}\right)^{2}}\right)^{2}+\sigma_{J}^{2}\left(\frac{A_{I} \frac{t_{J}}{t_{I}}}{\left(A_{I}+\frac{t_{J}}{t_{I}} A_{J}\right)^{2}}\right)^{2}.
\end{equation}

\section{Výsledky merania}

\subsection*{Kalibrácia detektora}
\indent Pre ďalšie meranie je nutné použiť žiarič u ktorého poznáme energetickú hodnotu maxima s najvyššou početnosťou, vďaka tomu budeme schopný vykonať kalibráciu mierky, ktorú použijeme po zvyšok merania. Detektor sme kalibrovali meraním energie incidentných dopadov $\alpha$-častíc pochádzajúcich z izotopu $^{241}$Am. Merali sme po dobu $t$ = 300 s pri tlaku $p$ = (10$\pm$5).10$^{-3}$ atm. Na kalibráciu sme použili znalosť energetického maxima emitovaných $\alpha$-častíc z izotopu Am, tým je 5485,7 keV.

\subsection*{Absolútna aktivita žiariča $^{241}$Am}
\indent Vzdialenosť od žiariča k detektoru sme merali posuvným meradlom, ale chybu sme odhadli na 1 mm, pretože vzorka Am bola zasadená do plastového obalu a podobne detektor bol z bokov chránený, to nám znemožnilo určiť vzdialenosť $l$ presnejšie. Chyba $\sigma_l$ je nezanedbateľná, chybu plochy $S$ nepoznáme, uvažujeme veľkosť plochy $S$ ako dokonale presný údaj, teda \\
\begin{equation*}
  S = 900\:\mathrm{mm}^{2}.
\end{equation*}
Podobne zanedbávame chybu počtu nameraných častíc $N$ a času $t$, ich relatívne príspevky do celkovej chyby absolútnej aktivity žiariča sú zanedbateľné. Definujeme teda novú premennú
\begin{equation}
  B = \frac{A}{l^2} = \frac{N}{t} \frac{4\pi}{S}
\end{equation}
Namerané hodnoty počtu dopadov $\alpha$-častíc s dĺžkou merania pri danom tlaku sú v tabuľke 1. Chybu $\sigma_{B}$ sme určili pomocou metódy priameho merania s Besselovou korekciou a s korekciou t-distribúcie [3]. Posledné dve hodnoty sme ale do priemeru nezahŕňali, pretože neboli v intervale 3$\sigma$, kde šírku intervalu sme vždy určili pomocou nameraných údajov za nižšieho tlaku. To možno zdôvodniť tak, že pri vyššom tlaku dôjde k vyšším stratám $\alpha$-častíc vo vzduchu. Chybu $\sigma_{A}$ určíme (4.16)[1] ako
\begin{equation}
  \sigma_{A } = \sqrt{l^{4}\sigma_{B}^{2}+\left(2 B l\right)^2\sigma_{l}^{2}}.
\end{equation}
Určená absolútna aktivita izotopu $^{241}$Am je \\
\begin{equation*}
  A = (7500 \pm 400)\:\mathrm{bq}.
\end{equation*}
%
\begin{table}[H]
  \begin{center}
    \caption{Početnosť dopadu $\alpha$-častíc a dĺžka merania v závislosti na okolitom tlaku.}
    \begin{tabular}{|c|r|c|c|}
      \hline
      $p.10^{-3}\:[\mathrm{atm}]$ & \multicolumn{1}{c|}{$N.10^3$} & $t\:[\mathrm{s}]$ & $B.10^{6}\:[\mathrm{s}^{-1}\mathrm{m}^{2}]$ \\ \hline \hline
      50	$\pm$	50                   & 161,3                         & 367               & 6,14                                        \\
      100	$\pm$	50                  & 66,1                          & 149               & 6,19                                        \\
      200	$\pm$	50                  & 108,9                         & 247               & 6,16                                        \\
      300	$\pm$	50                  & 98,8                          & 223               & 6,19                                        \\
      400	$\pm$	50                  & 156,4                         & 359               & 6,09                                        \\
      500	$\pm$	50                  & 98,9                          & 225               & 6,13                                        \\
      600	$\pm$	50                  & 90,0                          & 208               & 6,04                                        \\
      700	$\pm$	50                  & 115,9                         & 265               & 6,11                                        \\
      800	$\pm$	50                  & 103,2                         & 234               & 6,16                                        \\
      900	$\pm$	50                  & 88,1                          & 236               & 5,22                                        \\
      980	$\pm$	50                  & 20,9                          & 302               & 0,97                                        \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\subsection*{Ionizačné straty kinetickej energie $\alpha$-častíc}
\indent Pomocou polovodičového detektoru sme merali počet dopadov $\alpha$-častíc v závislosti na zmene tlaku. V aparatúre sme najprv pomocou olejového čerpadla dostatočne znížili tlak. Dostali sme sa k hodnote $p = 0,05\:\mathrm{atm}$. Merali sme počet dopadov približne po dobu $t = 200\:\mathrm{s}$, pri zázname dopadov sme mali v prostredí programu na vyhodnocovanie dát preložený centroid cez početnosť dopadov pre jednotlivé hodnoty energií, a to pomocou metódy najmenších štvorcov. V prostredí bola tiež priložená chyba tejto regresie (viď. zápis 'uncertainity'). Snažili sme sa udržať túto chybu okolo hodnoty $0,32$. Po dobu dlhšiu ako 200 s sme už nepozorovali zlepšenie tejto regresie. Namerané hodnoty sú v tabuľke 2. Chyby $T$ a $\Delta T$ boli určené podľa pološírky $\Gamma$ (FWHM) [2] ako
\begin{equation}
  \Gamma = \frac{FWHM}{2\sqrt{2 \ln 2}}.
\end{equation}
Závislosť smeny úbytku kinetickej energie na tlaku je vynesená na obr. 1. Našou úlohou je nájsť deriváciu závislosti kinetickej energie častíc na prepočítanej vzdialenosti $x_a$, t.j. $\frac{d T}{d x}$. Prvá derivácia $T(x_a)$ (13) je
\begin{equation}
  -\frac{d T}{d x} = - \frac{T_i-T_{i+1}}{x_{i}-x_{i+1}}
\end{equation}
Chybu každého nového bodu tejto funkcie označím ako $\sigma_d$ a určíme ju (4.16)[1] ako
\begin{equation}
  \sigma_{d }^2 = \left(\frac{1}{x_{i} - x_{i+1}}\right)^2(\sigma_{T_i}^{2}+\sigma_{T_{i+1}}^{2})+\left(\frac{T_{i} - T_{2}}{\left(x_{i} - x_{i+1}\right)^{2}}\right)^2(\sigma_{x_i}^{2} + \sigma_{x_{i+1}}^{2})
\end{equation}
Takto určené hodnoty funkcie $-\frac{d T}{d x}$ máme v tabuľke 2. Závislosť energie častíc, $T$ na jej derivácii podľa vzdialenosti, $-\frac{d T}{d x}$ sme vyniesli do grafu a porovnali ich s empirickou formulou (obr. 2).
%
\begin{table}[H]
  \begin{center}
    \caption{Tabuľka určených kinetických energii $\alpha$-častíc a ich strát.}
    \begin{tabular}{|c|c|c|c|c|c|}
      \hline
      $p.10^{-3}\:[\mathrm{atm}]$ & $FWHM\:[\mathrm{keV}]$ & $x_a\:[\mathrm{cm}]$ & $T\:[\mathrm{MeV}]$ & $\Delta T\:[\mathrm{MeV}]$ & $-\frac{d T}{d x}\:[\mathrm{MeV\cdot cm}^{-1}]$ \\ \hline \hline
      50	$\pm$	50                   & 104                    & 0,18	$\pm$	0,01        & 5,39	$\pm$	0,07       & 0,46	$\pm$	0,07              & 1,066	$\pm$	0,55                                  \\
      100	$\pm$	50                  & 106                    & 0,36	$\pm$	0,02        & 5,20	$\pm$	0,07       & 0,65	$\pm$	0,07              & 1,037	$\pm$	0,32                                  \\
      200	$\pm$	50                  & 121                    & 0,71	$\pm$	0,04        & 4,82	$\pm$	0,08       & 1,02	$\pm$	0,08              & 1,140	$\pm$	0,42                                  \\
      300	$\pm$	50                  & 147                    & 1,07	$\pm$	0,06        & 4,42	$\pm$	0,09       & 1,43	$\pm$	0,09              & 1,089	$\pm$	0,52                                  \\
      400	$\pm$	50                  & 177                    & 1,43	$\pm$	0,08        & 4,03	$\pm$	0,11       & 1,82	$\pm$	0,11              & 1,244	$\pm$	0,69                                  \\
      500	$\pm$	50                  & 224                    & 1,79	$\pm$	0,10        & 3,58	$\pm$	0,14       & 2,26	$\pm$	0,14              & 1,316	$\pm$	0,87                                  \\
      600	$\pm$	50                  & 275                    & 2,14	$\pm$	0,12        & 3,11	$\pm$	0,18       & 2,73	$\pm$	0,18              & 1,364	$\pm$	1,08                                  \\
      700	$\pm$	50                  & 348                    & 2,50	$\pm$	0,14        & 2,63	$\pm$	0,22       & 3,22	$\pm$	0,22              & 1,707	$\pm$	1,48                                  \\
      800	$\pm$	50                  & 463                    & 2,86	$\pm$	0,16        & 2,02	$\pm$	0,30       & 3,83	$\pm$	0,30              & 1,841	$\pm$	1,78                                  \\
      900	$\pm$	50                  & 513                    & 3,21	$\pm$	0,19        & 1,36	$\pm$	0,33       & 4,49	$\pm$	0,33              & 1,091	$\pm$	1,73                                  \\
      980	$\pm$	50                  & 336                    & 3,50	$\pm$	0,20        & 1,05	$\pm$	0,22       & 4,80	$\pm$	0,22              & -                                               \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
%
\begin{figure}[H]
  \begin{center}
    \includegraphics[scale=0.8]{Graph01.png}
    \caption{Úbytok kinetickej energie $\alpha$-častíc v závislosti na okolitom tlaku.}
  \end{center}
\end{figure}
%
%
\begin{figure}[H]
  \begin{center}
    \includegraphics[scale=0.8]{Graph03.png}
    \caption{Závislosť špecifickej ionizačnej straty na kinetickej energii $\alpha$-častíc.}
  \end{center}
\end{figure}
%
\subsection*{Pomerná aktivita}
\indent Meraním s rovnakou aparatúrou, no so zdrojom v najbližšej možnej polohe pri detektore a s maximálne evakuovanou aparatúrou sme častice zaznamenávali po dobu $818\:\mathrm{s}$. Podarilo sa nám identifikovať dve jasné maximá v energetickom spektre, na hodnotách zvlášť označených $T_{239}$ a $T_{238}$
\begin{equation*}
  T_{239} = (5220  \pm 60)\:\mathrm{MeV},
\end{equation*}
\begin{equation*}
  T_{238} = (5600  \pm 60)\:\mathrm{MeV}.
\end{equation*}
Tieto hodnoty sme vedeli priradiť k izotopom na základe poznatku ich skutočných hodnôt $T_{239}$ a $T_{238}$, tie budem porovnávať v diskusii. Ich pomerné aktivity boli určené ako
\begin{equation*}
  A_{239} = (947  \pm 60)\:\mathrm{bq},
\end{equation*}
\begin{equation*}
  A_{238} = (8,47  \pm 6)\:\mathrm{bq}.
\end{equation*}
Na základe (8) a (9) sme určili ich pomernú aktivitu ako
\begin{equation*}
  p_{238} = (3,3  \pm 0,1).10^{-5}.
\end{equation*}
\section{Diskusia}
\indent Pre kalibráciu mierky s použitím izotopu Am, sme potrebovali dva body, nulu a špecifickú hodnotu na ktorej je emitovaných najviac $\alpha$-častíc. Avšak dôsledkom geometrie a tiež nenulovým tlakom v aparatúre je nanajvýš pravdepodobné, že toto maximum bolo posunuté smerom k nižším hodnotám kinetickej energie. To znamená, že naprieč celým meraním pracujeme s istou systematickou chybou, ktorú môžeme iba odhadovať. Z grafu na obr. 1 možno hrubo túto chybu odhadnúť, odhadujeme ju na 2\%. \\
\indent Pri hľadaní maxima v danom energetickom rozdelení $\alpha$-častíc pri zvolenom tlaku sme v regresii uplatnili normálne rozdelenie. Avšak normálne rozdelenie je symetrickou funkciou, vzhľadom k $x=x_0$. Lenže pri vyššom tlaku, t.j. $p\in [0.5, 0.8]\:\mathrm{atm}$ toto rozdelenie bolo asymetrické a počas merania sme si mohli pozorovať, že maximum získané z tejto regresie neodpovedá skutočnému maximu. Dovolíme si tvrdiť, že relatívny príspevok tejto chyby je malý a teda môžeme ho zanedbať, t.j. menej ako 1\%. V opačnom prípade by sa táto chyba prejavila na určenom úbytku kinetickej energie (obr. 1). \\
\indent Namerané špecifické ionizačné straty pri normálnom tlaku odpovedajú v rámci uvedených experimentálnych chyb (tabuľka 2) pre $T > 4\:\mathrm{MeV}$ empirickej závislosti (7), ktorá platí práve pre $T \in [4,7]\:\mathrm{MeV}$. Pre nižšie energie je úbytok kinetickej vyšší, ako to predpovedá empirická formula (obr. 2). \\
\indent Kinetické energie izotopov $^{238}$Pu a $^{239}$Pu neodpovedajú experimentálne zisteným hodnotám [4] $T_{238} = 5593\:\mathrm{MeV}$ a $T_{239} = 5156\:\mathrm{MeV}$ v prípade rádioizotopu $^{239}$Pu na intervale $1\sigma$, na $2\sigma$ sa tieto hodnoty už zhodujú. V prípade izotopu $^{238}$Pu môžeme hovoriť o dobrej zhode. \\
\indent Relatívne zastúpenie izotopu $^{238}$Pu v izotope $^{239}$Pu bolo určené s presnosťou lepšou ako 10\% a to s relatívnou chybou 3\%. Pri rádovo dlhšom meraní odhadujeme spresnenie výsledku, avšak to bolo nad rámec časovej dotácie. \\

\section{Záver}
\indent Vykonali sme kalibráciu $\alpha$-spektrometru použitím izotopu $^{241}$Am. \\
Absolútna aktivita žiariča $^{241}$Am je
\begin{equation*}
  A = (7500 \pm 400)\:\mathrm{bq}.
\end{equation*}
Zmerali sme závislosť ionizačnej straty pri zmene tlaku a následne sme určili špecifické ionizačné straty pri normálnom tlaku (viď. tabuľka 2).
Relatívne zastúpenie izotopu $^{238}$Pu v izotope $^{239}$Pu bolo určené ako
\begin{equation*}
  p_{238} = (3,3  \pm 0,1).10^{-5}.
\end{equation*}
% \section{Literatúra}
\begin{thebibliography}{}
  \bibitem{1}
  Hughes, I. and Hase, T. (2014). Measurements and their Uncertainties. Oxford: OUP Oxford, p.43.
  \bibitem{2}
  Študijný text k úlohe A5, (dňa 14.5.2019) \url{https://physics.mff.cuni.cz/vyuka/zfp/_media/zadani/texty/txt_405.pdf}
  \bibitem{3}
  Grigelionis, B. (2013). Student's t-Distribution and Related Stochastic Processes. Berlin, Heidelberg: Springer Berlin Heidelberg, p.1.
  \bibitem{4}
  Mietelski, J. and Was, B. (1995). Plutonium from Chernobyl in Poland. Applied Radiation and Isotopes, 46(11), pp.1203-1211.
\end{thebibliography}
\end{document}